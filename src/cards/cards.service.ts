import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Card } from './entities/card.entity';

@Injectable()
export class CardsService extends TypeOrmCrudService<Card> {
    constructor(
        @InjectRepository(Card)
        private cardsRepository: Repository<Card>) { super(cardsRepository); }
}
