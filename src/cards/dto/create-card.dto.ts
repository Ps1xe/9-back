import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUUID } from 'class-validator';

export class CreateCardDto {
  @ApiProperty({ type: String })
  @IsString()
  name: string;

  
  @ApiProperty({ type: String })
  @IsUUID()
  columnId: string;
}
