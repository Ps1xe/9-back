import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Column } from './entities/column.entity';

@Injectable()
export class ColumnsService extends TypeOrmCrudService<Column> {

    constructor(
        @InjectRepository(Column)
        private columnsRepository: Repository<Column>) { super(columnsRepository) }
}
